import React from "react"
import { createRoot } from "react-dom/client"
import { Provider } from "react-redux"
import { store } from "config"
import { Router } from "routes/Router"
import "./App.css"

const container = document.getElementById("root")!
export const root = createRoot(container)

root.render(
  <React.StrictMode>
    <Provider store={store}>
      <Router />
    </Provider>
  </React.StrictMode>
)
