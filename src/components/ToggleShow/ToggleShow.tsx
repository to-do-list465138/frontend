import { ToggleButton, Tooltip } from "@mui/material"
import { ToggleButtonGroup, Image } from "./ToggleShow.styles"
import { done, not_done, all } from "utils"
import { ToggleType } from "types"

const ToggleShow = (props: {
  setToggleState: (type: ToggleType) => void
  toggleState: ToggleType
}) => {
  const { setToggleState, toggleState } = props
  return (
    <ToggleButtonGroup
      exclusive
      onChange={(_, newState) =>
        setToggleState(newState ? { done: newState } : {})
      }
      value={toggleState.done}
    >
      <Tooltip title="done tasks">
        <ToggleButton value={"true"}>
          <Image src={done} />
        </ToggleButton>
      </Tooltip>
      <Tooltip title="alltasks">
        <ToggleButton value={"all"}>
          <Image src={all} />
        </ToggleButton>
      </Tooltip>
      <Tooltip title="remaining tasks">
        <ToggleButton value={"false"}>
          <Image src={not_done} />
        </ToggleButton>
      </Tooltip>
    </ToggleButtonGroup>
  )
}

export default ToggleShow
