import "@testing-library/jest-dom"
import { fireEvent, screen, render } from "config"
import { NewTask, SingleTask } from "components"

describe("NewTask tests", () => {
  beforeEach(() => {
    render(<NewTask />)
  })

  it("new task textfield is clear on render", () => {
    expect(screen.getByTestId("new_task_textfield").textContent).toMatch("")
  })

  it("input text changes text", () => {
    const input = screen
      .getByTestId("new_task_textfield")
      .querySelector("input") as HTMLInputElement

    expect(input).toBeInTheDocument()
    fireEvent.change(input, { target: { value: "Test task!" } })
    expect(input.value).toMatch("Test task!")
  })

  it("add button adds new task", () => {
    const input = screen
      .getByTestId("new_task_textfield")
      .querySelector("input") as HTMLInputElement

    fireEvent.change(input, { target: { value: "Test task!" } })
    fireEvent.click(screen.getByTestId("new_task_button"))
    expect(input.value).toMatch("")
  })
})

describe("SingleTask tests", () => {
  const data = {
    id: 123,
    done: false,
    content: "Test Task!",
  }
  const mockedHandlePatch = jest.fn()
  const mockedHandleDelete = jest.fn()

  beforeEach(() => {
    render(
      <SingleTask
        data={data}
        handlePatch={mockedHandlePatch}
        handleDelete={mockedHandleDelete}
      />
    )
  })

  it("renders content correctly", () => {
    expect(screen.getByTestId("task_content").textContent).toMatch("Test Task!")
  })

  it("done button marks task as done", () => {
    fireEvent.click(screen.getByTestId("patch_button"))
    expect(mockedHandlePatch).toHaveBeenCalledTimes(1)
    expect(mockedHandlePatch).toHaveBeenCalledWith(data.id, data.done)
  })

  it("delete button deletes task", () => {
    fireEvent.click(screen.getByTestId("delete_button"))
    expect(mockedHandleDelete).toHaveBeenCalledTimes(1)
    expect(mockedHandleDelete).toHaveBeenCalledWith(data.id)
  })
})
