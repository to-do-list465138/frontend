import { configureStore, combineReducers } from '@reduxjs/toolkit';
import { tasksApi } from 'api';

const rootReducer = combineReducers({
  [tasksApi.reducerPath]: tasksApi.reducer,
})

export const store = configureStore({
  reducer: rootReducer,
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware().concat(tasksApi.middleware),
})