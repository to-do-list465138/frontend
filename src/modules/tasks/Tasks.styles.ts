import styled from "@emotion/styled"

export const Container = styled("div")({
  display: "flex",
  width: "100%",
  height: "100vh",
  flexDirection: "column",
  justifyContent: "center",
  alignItems: "center",
})

export const Box = styled("div")({
  display: "flex",
  flexDirection: "column",
  backgroundColor: "#f7f9f9",
  maxWidth: "500px",
  width: "50%",
  height: "75%",
  minHeight: "400px",
  padding: "20px",
  borderRadius: "10px",
  fontFamily: "'Handlee', cursive",
})

export const TopBar = styled("div")({
  display: "flex",
  flexDirection: "row",
  justifyContent: "space-around",
  alignItems: "center",
  margin: "-15px",
})

export const Title = styled("p")({
  fontSize: "36px",
})
